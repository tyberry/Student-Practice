This is going to be a 3 part exercise.

Aiming to keep it simple, so we are going to start with command line interfaces and curl calls/python requests

I highly recommend you watch these videos before you get started

https://www.youtube.com/watch?v=3tSq5Yo2e2o&t=158s

https://www.youtube.com/watch?v=q94B9n_2nf0&t=906s

https://www.youtube.com/watch?v=WxUVU0b95Oc&t=257s

Also check out this doc explaining argparse 

https://docs.python.org/3/library/argparse.html#action




Here are some hints.

1) Using Pycharm will make your life easier

2) chmod +x is going to come in handy

2) One of the 3 files has an error that allows the code to compile but runs nothing

3) To fix the problem run your files that work, analyze them all, and pinpoint/correct the mistake
