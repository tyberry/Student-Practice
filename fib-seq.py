#!/usr/bin/python3.9
import argparse

def fib(n):

    a, b = 0, 1
    for i in range(n):
        a, b = b, a+b

    return a


def main():
        parser = argparse.ArgumentParser()
        parser.add_argument("num", help="The number you want to be calculated", type=int)
        parser.add_argument("-o", "--output", help="Sends the results to a file ", action="store_true")

        args = parser.parse_args()

        results = fib(args.num)

        print("The "+str(args.num)+"th fib number is "+str(results))

        if args.output:
            f = open("fibonacci.txt", "w")
            f.write(str(results))


if __name__ == '__main__':
    main()

# See if you can figure out how to run this as a CLI
# This one is to mess around with we will come back to it

