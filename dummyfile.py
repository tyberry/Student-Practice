#!/usr/bin/python3.9
import argparse

def main():
        parser = argparse.ArgumentParser()
        group = parser.add_mutually_exclusive_group()
        group.add_argument("-v", "--version", help="This is where the description of the command goes", action="store_true")
        group.add_argument("-b", "--berry", help="This is where the description of the command goes", action="store_true")
        args = parser.parse_args()

        if args.version:
            print("Version")

        elif args.berry:
            print("berry")


if __name__ == '__main__':
    main()


# Initialize a variable in the format of 3.0.4.675 (use what ever numbers just keep the format)
# Substitute the variable you initialize for "Version"

# Add a description to the "help" portion of the code line 7 & 8

# Change my last name to your last name
# ex: -b would switch to the first letter of you lastname, Berry = -b, --berry; Garreth =  -g, --garreth; Williams = -w , --williams;